
package tema2_inc1;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextArea;

public class Coada extends Thread{

	private List<Client> lista=new ArrayList<Client>();
	private int idCoada;
	public static int id=0;
	private JLabel ceas;
	private PrintWriter writer;
	private JTextArea evenimenteTxt;

	public Coada(JLabel ceas, PrintWriter writer, JTextArea evenimenteTxt) {
		this.ceas=ceas;
		this.writer=writer;
		this.evenimenteTxt=evenimenteTxt;
	}
	public synchronized void addClient(Client c) {

		lista.add(c);
		notifyAll(); 
	}
	public synchronized void scadeTimpAsteptare() {
		Client cl;
		for (int j = 0; j < lista.size(); j++) {
			cl = lista.get(j);
			cl.setTimpAsteptare(cl.getTimpAsteptare() -1);
		}

	}
	public synchronized void removeClient() throws InterruptedException, FileNotFoundException, UnsupportedEncodingException {
		while(lista.size()==0)
			wait();
		Client c=lista.get(0);
		if(c.getTimpAsteptare()==0) {

			String m=ceas.getText();
			m+=" "+lista.get(0).getNrClient()+" a fost servit la "+getId();
			evenimenteTxt.append(m+"\n");
			System.out.println(m);
			//if(!(ceas.getText().equals("00m 00s")))
				lista.remove(0);
			notifyAll(); }
	}


	public synchronized String coadaToSting() {
		String res="";
		res=res+"//casa "+getId()+"//";
		for(Client c: lista ) {
			res=res+" (c: "+c.getNrClient()+", a: "+c.getTimpSosire()+", s: " +c.getTimpServire()+") ";
		}
		return res;
	}

	public void run() {
		try {
			while(true) {
				scadeTimpAsteptare();
				removeClient();
				sleep( (int) (Math.random()*4000) ); 
			}
		}catch (InterruptedException | FileNotFoundException | UnsupportedEncodingException e) {
			System.out.println("Intrerupere");
			System.out.println( e.toString()); 
		}
	}


	public synchronized int getLungime() {
		//notifyAll(); 
		return lista.size();
	}
	public synchronized int getTimpAsteptare() {
		notifyAll(); 
		int count=0;
		for(int i=0; i<lista.size(); i++) {
			count+=lista.get(i).getTimpAsteptare();
		}
		System.out.println("timp asteptare "+count);
		return count;
	}

	public  int astepta() {
		int res=0;
		if(lista.size()==0)
			return res;
		res=lista.get(lista.size()-1).getTimpAsteptare();

		return res;
	}

	public List<Client> getLista() {
		return lista;
	}

	public void setLista(List<Client> lista) {
		this.lista = lista;
	}

	public int getIdCoada() {
		return idCoada;
	}

	public void setIdCoada() {
		idCoada=id;
		id++;
	}



}
