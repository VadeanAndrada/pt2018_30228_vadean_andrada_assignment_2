package tema2_inc1;

import java.util.Random;

public class GeneratorClient {
	public synchronized Client getNewClient(int minSosire, int maxSosire, int minServire, int maxServire ) {
		
		Random servire=new Random();
		Random sosire=new Random();
		Random id=new Random();
		
		int range1=maxSosire-minSosire+1;
		int range2=maxServire-minServire+1;
		int rangeId=100-1+1;
		
		int arr=sosire.nextInt(range1)+minServire;
		int ser=servire.nextInt(range2)+minServire;
		int idC=id.nextInt(rangeId)+1;
		
		Client c=new Client(arr,ser,idC);
		
		return c;
	}
}
