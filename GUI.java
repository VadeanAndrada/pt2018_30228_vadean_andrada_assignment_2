package tema2_inc1;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;


public class GUI extends JFrame{
	private JFrame frame;
	private JPanel contentPane;
	private JLabel lblCozi,lblClienti,lblSosire,lblServire, lblTimp;
	private JTextField nrCozi, nrClienti, maxSosire, minSosire, maxServire, minServire, timpSimulare;
	private JButton start;
	private static JTextArea cozi, evenimenteTxt;
	private static String coziAfis;
	public JLabel ceas;
	public Timer timer;
	public PrintWriter writer;// = new PrintWriter("tema2.txt", "UTF-8");
	private Duration duration;//=Duration.ofMinutes(1);
	private LocalDateTime startTime= LocalDateTime.now();
	public GUI() {
		frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Tema 2");
		init();
		contentPane= new JPanel();
		contentPane.setLayout(null);
		contentPane.add(lblCozi);
		contentPane.add(lblClienti);
		contentPane.add(lblSosire);
		contentPane.add(lblServire);
		contentPane.add(nrCozi);
		contentPane.add(nrClienti);
		contentPane.add(maxSosire);
		contentPane.add(minSosire);
		contentPane.add(maxServire);
		contentPane.add(minServire);
		contentPane.add(start);
		contentPane.add(cozi);
		contentPane.add(lblTimp);
		contentPane.add(timpSimulare);
		contentPane.add(ceas);
		contentPane.add(evenimenteTxt);
		contentPane.setPreferredSize(new Dimension(1300,650));
		frame.setContentPane(contentPane);
		frame.pack();
		frame.setVisible(true);
	}
	public synchronized void init() {
		lblCozi=new JLabel("Numarul de cozi: ");
		lblCozi.setBounds(15, 15, 100, 40);

		lblClienti=new JLabel("Numarul de clienti: ");
		lblClienti.setBounds(15, 45, 150,40);

		lblSosire=new JLabel("Interval de sosire: ");
		lblSosire.setBounds(15,75, 200,40);

		lblServire=new JLabel("Interval de servire: ");
		lblServire.setBounds(15,105,200,40);

		nrCozi=new JTextField();
		nrCozi.setBounds(150, 25, 100, 20);

		nrClienti=new JTextField();
		nrClienti.setBounds(150,55, 100, 20);

		minSosire=new JTextField();
		minSosire.setBounds(150,85, 100, 20);

		maxSosire=new JTextField();
		maxSosire.setBounds(280, 85, 100,20);

		minServire=new JTextField();
		minServire.setBounds(150, 115,100,20);

		maxServire=new JTextField();
		maxServire.setBounds(280, 115, 100,20);

		lblTimp= new JLabel("Timp simulare:");
		lblTimp.setBounds(400, 15, 160, 40);

		timpSimulare= new JTextField();
		timpSimulare.setBounds(490,25, 100, 20);

		start=new JButton("Start");
		start.setBounds(15, 140,100, 40);

		ceas=new JLabel("");
		ceas.setBounds(430, 110, 100, 40);

		cozi=new JTextArea();
		cozi.setBounds(15, 190, 700, 380);
		
		evenimenteTxt=new JTextArea();
		evenimenteTxt.setBounds(800, 15, 400, 400);
		
		long timp=0;
		String t=timpSimulare.getText();
		try {
			timp=Integer.parseInt(t);
			//timp=Long.parseLong(t);
			//		duration= Duration.ofMinutes(1);
			//			setDuration(Duration.ofMinutes(1));
			// timp=timp*1000;
		}
		catch (Exception e1) {
			JOptionPane.showMessageDialog(frame,
					"Datele de intare nu sunt valide. INCEARCA CU MAI MULTA ATENTIE!");
		}
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setDuration(Duration.ofMinutes(1));
				LocalDateTime now = LocalDateTime.now();
				Duration runningTime = Duration.between(startTime, now);
				Duration timeLeft = duration.minus(runningTime);
				if (timeLeft.isZero() || timeLeft.isNegative()) {
					timeLeft = Duration.ZERO;
				}
				ceas.setText(format(timeLeft));
			}

		});


		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String s1=minSosire.getText();
				String s2=maxSosire.getText();
				String s3=minServire.getText();
				String s4=maxServire.getText();
				String s5=nrCozi.getText();
				String s6=nrClienti.getText();


				try {
				//	timer.start();
					timer.restart();
					int n1=Integer.parseInt(s1);//minSosire
					int n2=Integer.parseInt(s2);//maxSosire
					int n3=Integer.parseInt(s3);//minServire
					int n4=Integer.parseInt(s4);//maxServire
					int n5=Integer.parseInt(s5);//nrCozi					
					int n6=Integer.parseInt(s6);//nrClienti
					if(n1<0)
						n1=n1*(-1);
					if(n2<0)
						n2=n2*(-1);
					if(n3<0)
						n3=n3*(-1);
					if(n4<0)
						n4=n4*(-1);
					//PrintWriter writer = new PrintWriter("tema2.txt", "UTF-8");
					setWriter(writer);
					Coada c[]=new Coada[n5];
					for(int i=0; i<n5; i++) {
						c[i]=new Coada(getCeas(),getWriter(),getEvenimenteTxt());
						c[i].setIdCoada();
						c[i].start();
					}


					Casa q=new Casa(n5, c, n6,getCozi(),getCeas(),getWriter(),getEvenimenteTxt());
					q.setMinServire(n3);
					q.setMaxServire(n4);
					q.setMinSosire(n1);
					q.setMaxSosire(n2);
					timer.start();
					timer.toString();
					q.start();
					writer.close();	
					

				} catch (Exception e1) {
					JOptionPane.showMessageDialog(frame,
							"Datele de intare nu sunt valide. INCEARCA CU MAI MULTA ATENTIE!");

				}


			}

		});

	}
	protected synchronized String format(Duration duration) {
		long mins = duration.toMinutes();
		long seconds = duration.minusMinutes(mins).toMillis() / 1000;
		return String.format("%02dm %02ds", mins, seconds);
	}
	public JTextArea getCozi() {
		return cozi;
	}
	public void setCozi(JTextArea cozi) {
		this.cozi = cozi;
	}

	public static JTextArea getEvenimenteTxt() {
		return evenimenteTxt;
	}
	public static void setEvenimenteTxt(JTextArea evenimenteTxt) {
		GUI.evenimenteTxt = evenimenteTxt;
	}
	public PrintWriter getWriter() {
		return writer;
	}
	public void setWriter(PrintWriter writer) {
		this.writer = writer;
	}
	public JLabel getCeas() {
		return ceas;
	}

	public Duration getDuration() {
		return duration;
	}
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	public void setCeas(JLabel ceas) {
		this.ceas = ceas;
	}
	public static String getCoziAfis() {
		return coziAfis;
	}
	public void setCoziAfis(String coziAfis) {
		this.coziAfis = coziAfis;
	}
	public static void main(String[] Args) {
		Runnable r = new Runnable() {
			public void run() {
				new GUI();
			}
		};
		EventQueue.invokeLater(r);
	}

}
