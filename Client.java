package tema2_inc1;

public class Client {
	private int timpSosire;
	private int timpServire;
	private int timpAsteptare;
	private int nrClient;
	public Client() {
		
	}
	public Client(int timpSosire, int timpServire, int id) {
		this.timpServire=timpServire;
		this.timpSosire=timpSosire;
		nrClient=id;
	}
	
	
	public int getTimpSosire() {
		return timpSosire;
	}
	public void setTimpSosire(int timpSosire) {
		this.timpSosire = timpSosire;
	}
	public int getTimpServire() {
		return timpServire;
	}
	public void setTimpServire(int timpServire) {
		this.timpServire = timpServire;
	}
	public int getNrClient() {
		return nrClient;
	}
	public void setNrClient(int nrClient) {
		this.nrClient = nrClient;
	}
	public int getTimpAsteptare() {
		return timpAsteptare;
	}
	public void setTimpAsteptare(int timpAsteptare) {
		this.timpAsteptare = timpAsteptare;
	}
	

	
	
}
