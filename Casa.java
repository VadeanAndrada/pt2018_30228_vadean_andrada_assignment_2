
package tema2_inc1;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;
import javax.swing.JTextArea;

public class Casa extends Thread{
	private Coada coada[];
	private int nrCozi;
	private int clienti;
	private int minSosire,maxSosire,minServire,maxServire;
	private String afisare;
	private JTextArea textArea;
	private JLabel ceas;
	private PrintWriter writer;
	private JTextArea evenimenteTxt;

	public Casa(int nrCozi, Coada coada[],int clienti,JTextArea textArea,JLabel ceas,PrintWriter writer,JTextArea evenimenteTxt) {
		this.ceas=ceas;
		this.textArea=textArea;
		this.nrCozi=nrCozi;
		this.coada=new Coada[nrCozi];
		this.clienti=clienti;
		int n=0;
		this.evenimenteTxt=evenimenteTxt;
		this.writer=writer;
		for(int i=0; i<nrCozi;i++) {
			this.coada[i]=coada[i];
			coada[i].setIdCoada();
		}

	}

	public synchronized int alegeCoada() {
		int min=coada[0].getTimpAsteptare();
		int index=0, i=0;
		for(Coada c: coada) {
			if(min>c.getTimpAsteptare()) {
				min=c.getTimpAsteptare();
				index=i;
			}
			i++;
		}

		return index;
	}
	public synchronized int alegeCoadaSize() {
		int min=coada[0].getLungime();
		int index=0,i=0;
		Coada res=coada[0];
		for(Coada c: coada) {
			if(min>c.getLungime()) {
				min=c.getLungime();
				index=i;
			}
			i++;
		}
		System.out.println(coada[index].getIdCoada()+" are lungimea "+min);
		return index;
	}

	public void run() {
		try {
			GeneratorClient g=new GeneratorClient();
			int index=0;
			while(index<clienti&&(!(ceas.getText().equals("00m 00s")))) {
				index++;
				Client c=g.getNewClient(getMinSosire(),getMaxSosire(),getMinServire(),getMaxServire());
				int q=alegeCoada();
				c.setTimpAsteptare(c.getTimpServire()+coada[q].astepta());
				if(!(ceas.getText().equals("00m 00s")))
					coada[q].addClient(c);

				String m =ceas.getText();
				m+=" "+coada[q].getId()+" c: "+c.getNrClient()+" s: "+c.getTimpServire();
				System.out.println(m);
				evenimenteTxt.append(m+"\n");
				afiseaza();
				sleep( (int) (Math.random()*4000) ); 
			}
			while(true) {
				deseneaza();
				sleep( (int) (Math.random()*4000) );
			}
		}catch( InterruptedException e ){
			System.out.println(e.toString());	
		} 
	}

	public synchronized void  deseneaza() {
		for(int i=0; i<nrCozi; i++) {
			if(coada[i].getLungime()>0) {
				afiseaza();
			}
		}
	}

	public synchronized void afiseaza() {
		textArea.setText("");
		String afisare[]= {};

		List<String> af=new ArrayList<String>();
		for(int i=0; i<nrCozi;i++) {
			if(coada[i].getLista().size()>0)
			{
				af.add(coada[i].coadaToSting());
			}
		}
		for(String s:af) {
			textArea.append(s+"\n");
		}
	}
	public synchronized String getAfisare() {
		return afisare;
	}

	public synchronized void setAfisare(String afisare) {
		this.afisare = afisare;
	}


	public void setMinSosire(int minSosire) {
		this.minSosire = minSosire;
	}

	public void setMaxSosire(int maxSosire) {
		this.maxSosire = maxSosire;
	}

	public void setMinServire(int minServire) {
		this.minServire = minServire;
	}

	public void setMaxServire(int maxServire) {
		this.maxServire = maxServire;
	}
	public Coada[] getCoada() {
		return coada;
	}
	public void setCoada(Coada[] coada) {
		this.coada = coada;
	}
	public int getMinSosire() {
		return minSosire;
	}

	public int getMaxSosire() {
		return maxSosire;
	}

	public int getMinServire() {
		return minServire;
	}

	public int getMaxServire() {
		return maxServire;
	}

	public int getNrCozi() {
		return nrCozi;
	}
	public void setNrCozi(int nrCozi) {
		this.nrCozi = nrCozi;
	}
	public int getClienti() {
		return clienti;
	}
	public void setClienti(int clienti) {
		this.clienti = clienti;
	}	

}
